#include<iostream>
#include<string>
#include<vector>
#include<stdexcept>


using namespace std;

int main(){

    vector<string> names(5);

    names.at(0) = "Jhon";
    names.at(1) = "Jeol";
    names.at(2) = "Jaras";
    names.at(3) = "Jinet";
    names.at(4) = "Gof";

    for (string name : names){
        cout << name <<endl;
    }

    try
    {
          names.at(5) = "RGG";
    }
    catch(const out_of_range& err)
    {
        cout << err.what() << '\n';
    }
    
    //exception ?
    //names.at(5) = "RGG";



    return 0;
}