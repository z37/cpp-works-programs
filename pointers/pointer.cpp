#include<iostream>
using namespace std;


int main(){

    int myInt = 150;

    // challenge
    double myDouble = 3.14;

    // pointer  to myInt
    int* somePtr = &myInt; 

    // pointer to myDouble
    double* somePtr2 = &myDouble;

    cout << " myInt is originally " << myInt << endl;
    cout << " pointer holds value " << somePtr<<endl;
    cout << " pointer dereferenced "<< *somePtr<<endl;

    *somePtr = 200;

    cout << "myInt is now: " << myInt <<endl;
    cout<<endl;

    cout << " myDouble is originally " << myDouble << endl;
    cout << " pointer holds value " << somePtr2 <<endl;
    cout << " pointer dereferenced "<< *somePtr2<<endl;






    return 0;
}