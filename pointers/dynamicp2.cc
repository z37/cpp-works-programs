#include<iostream>
#include<string>
using namespace std;

#ifndef DOG_H
#define DOG_H

class dog
{
private:
    string name;
    string breed;
public:

    dog(string name, string breed);
    string getName() const;
    string getBreed() const;
};

#endif

dog::dog(string name, string breed){
    this-> name = name;
    this-> breed = breed;
}


string dog::getName() const{
    return name;
}

string dog::getBreed() const{
    return breed;
}

int main(){
    
    dog* myDogptr = new dog("Rover", "German Sheperd");
    dog* yourDogptr = new dog("Fido", "Beagle");


    // here we access the member functions with the pointer using the (->) operator
    cout << " using arrow member acces " << endl;
    cout << myDogptr->getName() << " - " << myDogptr->getBreed() << endl;
    cout << yourDogptr->getName() << " - " << yourDogptr->getBreed() << endl;


    cout << endl;


    // here we can use the dereferenced pointer with the dot operator to acces the member functions of the object because how is returning the value of the class.
    cout << " using dereferenced and the dot operator to access member functions of the class objects " << endl;
    cout << (*myDogptr).getName() << " - " << (*myDogptr).getBreed() << endl;
    cout << (*yourDogptr).getName() << " - " << (*yourDogptr).getBreed() << endl;


    delete myDogptr;
    delete yourDogptr;

    myDogptr   = nullptr;
    yourDogptr = nullptr;





    return 0;

}