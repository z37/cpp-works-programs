#include<iostream>
using namespace std;

int main(){

    
    // const array_Size = 5;

    // this is an other form instead of the const array_Size
    
    int arraySize;
    
    cout << " select the size of the array"<<endl;
    cin >> arraySize;

    int* myArray  = new int[arraySize];

    for (int i = 0; i < arraySize; i++){
        myArray[i] = i * 2;
    }

    for (int i = 0; i < arraySize ; i++){
        cout << myArray[i] << endl;
    }

    delete[] myArray;


    return 0;
}