#include<iostream>
using namespace std;

int main(){

    // this type of pointer is a raw pointer
    // when using new for pointer and then decide when to delete.
    // here, it sets dynamically allocated memory 
    int* myIntPtr = new int(123);
    /* other equivalent form is 
    int* myIntPtr = new int;
    *myIntPtr = 123;
    */

    cout << *myIntPtr << endl;

    bool* boolptr = new bool(true);

    cout <<boolalpha << *boolptr << endl;

    // deletes the memory allocation pointer, so the memory adress is no longer valid
    delete myIntPtr;
    // to set the pointer even do is not valid de memory adress it needs to be set to null pointer (at location zero), in order to prevent memory leak in a program
    myIntPtr = nullptr;
    

    delete boolptr;

    boolptr = nullptr;


    return 0;
}