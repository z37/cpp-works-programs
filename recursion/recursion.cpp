#include<iostream>
using namespace std;

void countDownFrom(int number);
int sumValues(int number);
long int Factorial(int number);

int main(){

    countDownFrom(10);

    int totalSum  = sumValues(10);
    long int fact = Factorial(6); 

    cout<< "the sum from 1 to 10 is " << totalSum<< endl;

    cout<< "the factorial of 6 is " << fact << endl;
    
    return 0;
}

void countDownFrom(int number){

    if(number >= 0){
        cout << number << endl;
        countDownFrom(number - 1);
    }

}

int sumValues(int number){

    if(number > 0)
    {
        return number + sumValues(number -1);
    }
    return number;
}

long int Factorial(int number){
    if(number == 1){
        return number;
    }
    else if(number > 1 ) {
        return number * Factorial(number -1 );
    }
    
    return number;
}