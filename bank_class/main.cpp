#include<iostream>
#include "bank.h"
using namespace std;

int main(){

    BankAccount myAccount("JAra JAra", 5000);
    BankAccount bobsAccount("Bob Robs");

    bobsAccount.deposit(500);
    cout << " Bob´s account balance is "<< bobsAccount.getBalance() << endl;

    bobsAccount.withdraw(1000);
    cout<< " Bob´s account is still " << bobsAccount.getBalance() << endl;

    cout << myAccount.getOwner() << " account contains "<< myAccount.getBalance() << endl;

    return 0;
    
}