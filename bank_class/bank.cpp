#include"bank.h"
#include<iostream>
#include<string>
using namespace std;

BankAccount::BankAccount(string owner){
    this-> owner = owner;
    this->balance = 0;

}
BankAccount::BankAccount(string owner, int balance){
    this-> owner = owner;
    if( balance <= 0){
        cout << "You must enter a balance > than 0 " << endl;
    } else{
        this-> balance = balance;
    }
}

void BankAccount::deposit(int amount){
    
    if(balance > 0){
        balance += amount; 
    } else {
        cout << " The amount of the deposit needs to be > than 0"<< endl;
    }

}
void BankAccount::withdraw(int amount){

    if(amount > 0 && amount <= balance ){
        balance -= amount;
    } else{
        cout<< " The withdraw need to be less or equal to the balance in the account" << endl;
    }

}

string BankAccount::getOwner() const{
    return owner;
}

int BankAccount::getBalance() const{
    return balance;
}