#ifndef list_hpp
#define list_hpp

class list{ // remember that since, we have pure virtual methods we cannot instantiate the class List directly in main

public:
    virtual void add(int newEntry) = 0; // pure virtual function
    virtual void add(int newEntry, int position) = 0;
    virtual void set(int newEntry, int position) = 0;

    virtual bool contain(int newEntry) const = 0;
    virtual int find(int entry) const = 0;
    virtual int remove(int position) = 0;
    virtual void makeEmpty() = 0;

    virtual int size() const = 0;
    virtual bool isEmpty() const = 0;
    virtual void printList() const = 0;

    
};

#endif 