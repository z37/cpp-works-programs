#include<iostream>
#include"list.hpp"
#include"arraylist.hpp"
using namespace std;



int main(){

    arraylist myList;

    for (int i = 0; i < 15; i++){
        myList.add(i * 10);
    }

    myList.printList();

    cout << endl << endl;
    cout << "Size is: " << myList.size() << endl<< endl;

    myList.add(555, 15);

    myList.printList();
    cout << "Size is now: " << myList.size() << endl << endl;

    myList.set(987, 3);

    myList.printList();

    // out of range function works

    myList.add(100);

    return 0;
}
 
