#include<iostream>
#include<fstream>
#include<iomanip>
using namespace std;

void printFormatted( ofstream& outfile, int numberOfRepetion);

int main(){

    ofstream outfile;
    int numberFor;

    cout << "Enter de number of repetition "<< endl;
    cin >> numberFor;
    
    cout << "writing to file "<< endl;

    // create file txt
    outfile.open("output.txt");

    outfile << fixed << showpoint;
    cout << fixed << showpoint;

    // writing in file txt
    printFormatted(outfile, numberFor);  
    
    // closing file
    outfile.close();

    cout << "Done!\n";


    return 0;
}


void printFormatted( ofstream& outfile, int numberOfRepetion){
    
    for (int i = 1; i <= numberOfRepetion; i++){
        outfile << setw(12) << setprecision(2) << (i * 5.7575) << setw(12) << setprecision(3) << (i * 3.1416) << endl;

       cout << setw(12) << setprecision(2) << (i * 5.7575) << setw(12) << setprecision(3) << (i * 3.1416) << endl;
    }




}