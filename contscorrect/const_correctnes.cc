#include<iostream>
using namespace std;


// non-const pointer to non-const data
void noConst();
// const pointer to non-const data
void cp2ncd();
// non-const pointer to const data
void ncp2cd();
// const pointer to const data
void cp2cd();



int main() {

    noConst();
    cout << endl;

    cp2ncd();
    cout << endl;

    ncp2cd();
    cout << endl;
    
    cp2cd();
    cout << endl;

    return 0;
}

 
// non-const pointer to non-const data
// both pointer and the data pointing can be changed
void noConst(){
    cout << "In noConst" << endl; 

    int* intPtr = new int(50);

    cout << "\toring value : "<< *intPtr << endl;

    *intPtr = 100;
    cout << "\tchange value : "<< *intPtr << endl;

    delete intPtr; // delete the current dynamic int

    intPtr = new int(125);
    cout << "\tnew integer entirely: " << *intPtr << endl;

    delete intPtr;
} 

// const pointer to non-const data
// the pointer cannot be pointed into a new direction only the data pointing at can be changed
void cp2ncd(){

    cout << "In cp2ncd " << endl;

    // const pointer to an integer
    int* const intPtr = new int(100);
    cout << "\toring value : "<< *intPtr << endl;

    *intPtr = 250;
    cout << "\tchange value : "<< *intPtr << endl;

    delete intPtr;

    // this would be an error because the pointer is const.
    //intPtr = new int(222);

}

// non-const pointer to const data
// the pointer can be changed to point into a new direction but the data pointing at cannot be changed
void ncp2cd(){

    cout << "In ncp2cd" << endl;
    // is read from right to left
    //  const int <- pointer
    const int* intPtr = new int(500);

    cout << "\toring value : " << *intPtr << endl;

    delete intPtr;

    intPtr = new int(100);  // ok!

    // *intPtr = 15000; this is an error because the data is const.

    delete intPtr;
}
// const pointer to const data
// pointer is const and data pointing at.
void cp2cd(){

    const int* const intPtr = new int(5000);

    cout << "inside cp2cd" << endl;
    cout << "\torig value : "<< *intPtr << endl;

    // no change allowed
    // *intPtr = 1212;  this is an error besacuse data is const
    // intPtr = new int(1212); also this is an error, the pointer is const 

    delete intPtr;
}