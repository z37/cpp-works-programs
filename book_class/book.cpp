#include"book.h"
#include<iostream>
using namespace std;


book::book(string author,string title,string genre,int pages){

    this->author = author;
    this->title  = title;
    this->genre  = genre;
    this->pages  = pages;

}

string book::getAuthor() const{
    return author;
}    

string book::getTitle() const{
    return title;
}

string book::getGenre() const{
    return genre;
}

int book::getnumPages() const{
    return pages;
}

// we have acces to data members but also que can call function members
void book::printBookDetails() const{
 
 cout << title << " by " << author << " has " << pages << " pages " << " and its genre is " << genre << endl;



}