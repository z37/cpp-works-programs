#include<iostream>
#include"book.h"
using namespace std;


// example of function in main
// void printBookDetails(const book& book);

int main(){

    book gameOfThrones("George Martin", "Game of Thrones", "Fantasy", 864);

    book mathBook("JAmes Stewart", "Calculus", "Math", 1392);

    book cppBook("Bjarne Stroustrup", "The c++ programing language", "Programming", 1376);

    // now we call the function for the various books

    gameOfThrones.printBookDetails();
    mathBook.printBookDetails();
    cppBook.printBookDetails();
    return 0;
}

//example of function in main
/*
// remark is a const reference, so the function do not modify the data members of the book class
void printBookDetails(const book& book){

    cout << book.getTitle() << " by " << book.getAuthor() << " has " << book.getnumPages() << " pages " << " and its genre is " << book.getGenre() << endl;

}
*/

