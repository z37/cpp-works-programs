#ifndef BOOK_H
#define BOOK_H

#include<iostream>
#include<string>
using namespace std;

class book
{
private:
    string author;
    string title;
    string genre;
    int pages;

public:

    // param. constructor
    book(string author,string title,string genre,int pages);

    string getAuthor() const;
    string getTitle() const;
    string getGenre() const;
    int getnumPages() const;

    void printBookDetails() const;
};


#endif