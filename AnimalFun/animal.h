#ifndef ANIMAL_H
#define ANIMAL_H

#include<iostream>
#include<string>
using namespace std;

class animal
{
private:
    string name;
    double weight;
public:
    animal(string name, double weight);
    string getName() const;
    void setName(string name);
    double getWeight() const;
    void setWeight(double weight);

    // late (dynamic) binding, is when the function call is bound to the function def. at runtime. So in c++ it requires the KEYWORD virtual in the base class.
    virtual string makeNoise() const = 0; // a pure virtual function

    // pure virtual function for dog derived class
    virtual string eat() const = 0;


    
};

animal::animal(string name, double weight){
    this->name   = name;
    this->weight = weight;
}

string animal::getName() const{
    return name;
}

void animal::setName(string name){
    this->name = name;
}

double animal::getWeight() const{
    return weight;
}

void animal::setWeight(double weight){
    this->weight = weight;
}

/* string animal::makeNoise() const{
    return "unknown";
} */


#endif