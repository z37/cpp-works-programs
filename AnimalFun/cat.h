#ifndef CAT_H
#define CAT_H

#include<iostream>
#include<string>
#include"animal.h"
using namespace std;


class cat: public animal{

public:
    cat(string name, double weight);
    void chaseMouse() const;
    string makeNoise() const;
    string eat() const;
    
};

cat::cat(string name, double weight) : animal(name, weight){
}


void cat::chaseMouse() const{
    cout << "I am chasing a mouse!\n";
}

string cat::makeNoise() const{
    return "Meow Meow!";
}

string cat::eat() const{
    return "tasty kitty food";
}

#endif