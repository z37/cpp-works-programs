#ifndef DOG_H
#define DOG_H


#include<iostream>
#include<string>
#include"animal.h"
using namespace std;

// this is a derived a dog class derived form animal class

//rea  dog class <-  animal class
class dog: public animal{
private:
    string breed;
public:
    dog(string name, double weight,string breed);
    string getBreed() const;
    void digHole() const;
    string makeNoise() const; // this is a function ovverriding, and only happens on a derived class
    void chaseCat() const ;

    string eat() const;
};

// we can pass the private data to the dog class by initializing
dog::dog(string name, double weight,string breed) : animal(name, weight){
    this-> breed = breed;
} 

string dog::getBreed() const{
    return breed;
}

void dog::digHole() const{
    cout << "I am digging a hole "<< endl;
}

string dog::makeNoise() const{
    return "Woof woof";
}

void dog::chaseCat() const {
    cout << "Here kitty kitty, get over here\n" ;
}

string dog::eat() const{
    return "I love dog food chum chum";
}


#endif