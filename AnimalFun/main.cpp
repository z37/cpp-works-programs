#include<iostream>
#include<string>
#include"animal.h"
#include"dog.h"
#include"cat.h"
using namespace std;



int main(){

   // animal myAnimal("Sam", 100);
    dog Dog("Rover", 80, "Greyhound");

    // since a dog is a derived class from animal, we can call an animal* pointer to point to a dog object, this is called a polymorphic reference 
    animal* dogPtr = new dog("rash", 80, "Golden Retriever");
    animal* catPrt = new cat("Fausto", 90);

    // with the pointer we can point to other derived classes.


    cout << "Make noise? " << dogPtr->makeNoise() << endl;
    cout << "Hungry? " << dogPtr->eat() << endl;

    //for cat derived class

    cout << "Cat noise? " << catPrt->makeNoise() << endl;
    cout << "Cat eating? " << catPrt->eat() << endl;

    // cast it to cat pointer, instead of adding the member function in animal.h and use inheritance and plymorphism
    //((cat*)catPrt)->chaseMouse(); // old fashion cast

    // the developers use the reinterpret_cast< *Pointer >
    (reinterpret_cast<cat*>(catPrt))->chaseMouse();
    //              casting at -> cat*, and pass the pointer that we are casting from (catPrt)
     /*
    cout << "Animal name: " << myAnimal.getName() << endl;
    cout << "Animal weight: " << myAnimal.getWeight() << endl;
    cout << "Animal noise: " << myAnimal.makeNoise() << endl;

    cout<< "Dog's name: " << Dog.getName() << endl;
    cout<< "Dog's weight: " << Dog.getWeight() << endl;
    cout << "Dog's noise: " << Dog.makeNoise() << endl;
    Dog.digHole();
    Dog.chaseCat();
    */
 
    delete dogPtr;
    dogPtr = nullptr;

    delete catPrt;
    catPrt = nullptr;


    return 0;
}