#ifndef arraystack_hpp
#define arraystack_hpp

#include<iostream>
#include"stack.hpp"
using namespace std;

class arraystack : public stack {
private:
    int* mArray;
    const int MAX_SIZE;
    int top;
public:
    arraystack(int s = 15) : MAX_SIZE(s){
        top = -1; // -1 indicates no elements in the stack
        mArray = new int[MAX_SIZE];
    }

    void push(int newEntry){
        if (top < MAX_SIZE -1){
            top++;
            mArray[top] = newEntry;
        } else {
            cout << "Error: Stack is full! Cannot push!" << endl;
        }
    }

    int pop() {
        int data = 0;
        if (!isEmpty()){
            data = mArray[top];
            top--;
        } else {
            cout << "Error: you cannot pop an empty stack!" << endl;
        }
        return data;
        
    }

    int peak() const {
        int data = 0;

        if (!isEmpty()){
            data = mArray[top];
        } else {
            cout << "The stack is empty!" << endl;
        }
        return data;
    }

    bool isEmpty() const {
        return top == -1;
    }

    void makeEmpty() {
        top = -1;
    }
};








#endif