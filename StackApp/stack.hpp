#ifndef stach_hpp
#define stack_hpp

class stack{


public:
    virtual void push(int newEntry) = 0;
    virtual int pop() = 0;
    virtual int peak() const = 0;
    virtual bool isEmpty() const = 0;
    virtual void makeEmpty() = 0;
    
};


#endif