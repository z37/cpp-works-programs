#include<iostream>
#include"arraystack.hpp"
using namespace std;

int main(){

    arraystack stack;
    arraystack stack2;

    for (int i = 0; i < 17; i++){
        stack.push(i);
    }

    while (!stack.isEmpty()){
        stack2.push(stack.pop());
    }
    
    cout << "Same order as original stack: " << endl;

    while (!stack2.isEmpty()){
        int data = stack2.pop();
        stack.push(data);
        cout << data << endl;
    }
    return 0;
}