#include<iostream>
#include<queue>
#include<string>
using namespace std;

int main(){
    
    
    queue<string> strArray;
    
    strArray.push("Jhon");
    strArray.push("Sally");
    strArray.push("Bob");
    strArray.push("Sam");
    strArray.push("Ali");
    strArray.push("Karen");


    while (!strArray.empty()){

        cout << strArray.front() << endl;
        strArray.pop(); // dequeue
    }
    
    return 0;

}