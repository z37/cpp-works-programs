#include<iostream>
#include<string>
#include<stack>
using namespace std;

void storeReverse(string origString, stack<char>& reverseStack);
bool isPlaindrome(string origString);
void printResult(string origString);

int main(){

    string strArray[5];
    
    strArray[0] = "racecar";
    strArray[1] = "fudge";
    strArray[2] = "civic";
    strArray[3] = "bob";
    strArray[4] = "dogs";

    for (auto string : strArray){
        printResult(string);
        cout << endl;
    }
    



    return 0;
}


void storeReverse(string origString, stack<char>& reverseStack){
    for (char c :  origString){
        reverseStack.push(c);
    }
}


bool isPlaindrome(string origString){
    stack<char> reverseStack;
    storeReverse(origString, reverseStack);

    bool result = true;

    if (origString.length() == reverseStack.size()){
        
        for (char c :  origString){
            char currChar = reverseStack.top();
            if (currChar != c){
                result = false;
                break;
        }
            reverseStack.pop();
        }    
    } else{
        result = false;
    }
    return result;
}


void printResult(string origString){
    cout << "Is " << origString << ", a palindrome\t" << boolalpha << isPlaindrome(origString) << endl;
}