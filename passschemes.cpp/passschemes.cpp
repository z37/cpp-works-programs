#include<iostream>
using namespace std;

void valueChanged1(int someNumber);
void valueChanged2(int& someNumber);
void threeTimesN(int numInput, int& numOutput);

int main(){

    int myNumber = 20;
    int myInputNumber;

    cout<< "before valueChanged1 call, myNumber is "<< myNumber<<endl;
    valueChanged1(myNumber);
    cout<< "after valueChanged1 call, myNumber is "<< myNumber<<endl;

    cout<<endl<<endl;

    cout<< "myNumber is currently: "<<myNumber<<endl;
    valueChanged2(myNumber);
    cout<<"After valueChanged2 call, myNumber is now:"<<myNumber<<endl;

    cout<<endl<<endl;

    
    threeTimesN(100,myInputNumber);
    cout<<"The new value of myInputNumber after threeTimesN call:"<<myInputNumber<<endl;



    return 0;
}


void valueChanged1(int someNumber){

    someNumber = 100;
    cout<<"someNumber in valueChanged1 is "<< someNumber<<endl;

}

void valueChanged2(int& someNumber){

    someNumber = 100;
    cout<<"Inside someNumber in valueChanged2, someNumber is "<< someNumber<<endl;

}

void threeTimesN(int numInput, int& numOutput){

    numOutput = numInput * 3;
}