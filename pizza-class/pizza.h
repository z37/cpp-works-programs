#ifndef PIZZA_H
#define PIZZA_H

#include<string>
#include<vector>
using namespace std;

class pizza{
    private:
        string name;
        int cost;
        int diameter;
        vector<string> toppings;
    public:
        pizza(string name, int cost, int diameter);

        void addTopping(string topping);

        string getName() const;

        int getDiameter() const;

        int getCost() const;

        void printToppings() const;

};



#endif