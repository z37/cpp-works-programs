#include<iostream>
#include"pizza.h"
#include<string>
using namespace std;

void printPizzaOrder(const pizza& pizza);

int main(){
    
    pizza myPizza("super duper", 12, 9);
    pizza mattsPizza("super looper", 7, 8);

    mattsPizza.addTopping("pepperoni");

    pizza special("super special", 12, 10);
        
    special.addTopping("pepperoni");
    special.addTopping("chedar");
    special.addTopping("lasagne");
    special.addTopping("coke");

    printPizzaOrder(myPizza);
    printPizzaOrder(mattsPizza);
    printPizzaOrder(special);



    return 0;
}

pizza::pizza(string name, int cost, int diameter){
    this-> name     = name;
    this-> cost     = cost;
    this-> diameter = diameter;
    toppings.push_back("Cheese");
}

void pizza::addTopping(string topping){
    toppings.push_back(topping);
    
}

string pizza::getName() const{
    return name;
}

int pizza::getCost() const{
    return cost;
}

int pizza::getDiameter() const{
    return diameter;
}


void pizza::printToppings() const{
  
    for (string topping : toppings ){
        cout << " toppings selected \t" << topping << endl;
    }
    

}


void printPizzaOrder(const pizza& pizza){

    cout << "Name: " << pizza.getName() << endl; 
    cout << "Diameter: " << pizza.getDiameter() << endl; 
    cout << "Cost $: " << pizza.getCost() << endl;
    cout << "Toppings: " << pizza.printToppings() << endl;  
    cout << endl;

}

