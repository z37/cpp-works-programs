#include<iostream>
#include<fstream>
#include<string>
#include"shop.h"
using namespace std;

int main(){

    shop itemFrequencies;
    ifstream infile;
    string tempName;

    infile.open("shopping.txt");

    if (!infile){
        cout << "Error opening file, try again." << endl;
        return 1;
    }

    while (!infile.eof()){
        infile >> tempName;
        itemFrequencies.addItem(tempName);
    }
    
    itemFrequencies.printFreq();

    infile.close();
    
    return 0;
}

