#ifndef SHOP_H
#define SHOP_H

#include<string>
#include<vector>
#include<iostream>
#include<iomanip>
using namespace std;


class shop{

private:
    vector<string> items;
    vector<int> frequencies;
    int indexOfItem(string item) const;
    
public:
    void addItem(string item);
    void printFreq() const;


};

void shop::addItem(string item){
    int index = indexOfItem(item);

    if (index != -1){
        // item already exists
        frequencies[index]++;
    }else{

        //new item
        items.push_back(item);
        frequencies.push_back(1);
    }

}

void shop::printFreq() const{
    for (int i = 0; i<items.size(); i++){
        cout << setw(12) << items[i] << setw(8)
        << frequencies[i] << endl;
    }
    
}

int shop::indexOfItem(string item) const{

    int resultIndex = -1;

    for (int i = 0; i<items.size(); i++){
        if (item == items.at(i)){
            
            resultIndex = i;
            break;
        }
        
    }
    
    return resultIndex;
}


#endif