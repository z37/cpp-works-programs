#include "circle.h"
#include<cmath>
using namespace std;

//const double Pi = 3.14159;
                 // this below is an initializer list.
                 // and allows to set a value of a constant
circle::circle() : Pi(3.14159), radius(1) {
    radius = 1;
}   

circle::circle(double radius): Pi(3.14159){
    this-> radius = radius;
}

double circle::getRadius() const{
    return radius;
}

double circle::setRadius(double radius){
    this->radius = radius;
}

double circle::circumference() const{
    return 2 * Pi * radius;
}

double circle::area() const{
    return Pi * pow(radius, 2);
}