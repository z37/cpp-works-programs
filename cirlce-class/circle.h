#ifndef circle_h
#define circle_h

class circle
{
private:
    double radius;
    double const Pi;
public:

    circle();
    circle(double radius);
    double getRadius() const;
    double setRadius(double radius);
    double circumference() const;
    double area() const;    
    
};

#endif