#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP


class rectangle{
    friend void changeData(rectangle& rect); // this is not a member function(method), because it is defined in main

    // friend for forard declaration
    friend class rectangle_helper;

    friend void printData(rectangle& rect);

    public:
    
        // const in some functions, so that they do not modify internal data(private data member; length and width)

        rectangle();
        rectangle(double length, double width);
        double getLength() const; // const for no modification of the internal data
        double getWidth() const;
        void setLength(double length);
        void setWidth(double width);
        double area() const;
        double perimeter() const;


        bool operator==(const rectangle& other) const;
        rectangle operator+(const rectangle& other) const;
        void operator=(const rectangle& other);
        bool operator!=(const rectangle& other) const;


    private:
        double length;
        double width;

};


// data members
rectangle::rectangle(){

    this-> length  = 1.0;
    this-> width   = 1.0;

}


// member functions

rectangle::rectangle(double length, double width){

    this-> length = length;
    this-> width  = width;
}

double rectangle::getLength() const{
    return length;
}

double rectangle::getWidth() const{
    return width;
}

void rectangle::setLength(double length) {
    this-> length = length;
}

void rectangle::setWidth(double width) {
    this-> width = width;
}

double rectangle::area() const{

    return length * width;
}

double rectangle::perimeter() const{
    return 2 * (length + width);
}

//operators

bool rectangle::operator==(const rectangle& other) const{
    return length == other.length && width == other.width;

}

rectangle rectangle::operator+(const rectangle& other) const{

    rectangle newRect(length + other.length, width + other.width);
    
    return newRect;

}

void rectangle::operator=(const rectangle& other){
    length = other.length;
    width  = other.width;
}

bool rectangle::operator!=(const rectangle& other) const{
    return length != other.length || width != other.width;

}

#endif