#include<iostream>
#include"rectangle.hpp"
using namespace std;

int main(){

    rectangle rect1(10, 20);
    rectangle rect2(50, 100);
    rectangle rect3(10, 20);
    rectangle resultRect;

    resultRect = rect1 + rect2;

    cout << " rect1 == rect3 ? " << boolalpha << (rect1 == rect3) << endl;

    cout << "resultRectangle length * width: " << resultRect.getLength() << " * " << resultRect.getWidth() << endl;
    cout << "resultRectangle area: "<< resultRect.area() << endl;


    cout << " rect1 != rect2 ? " << boolalpha << (rect1 != rect2) << endl;

    return 0;

}