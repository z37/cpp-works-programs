#include<iostream>
#include<stdexcept>
#include"warp.h"
using namespace std;


void warpTest(int temperature);

int main(){

    cout << "Testing" << endl;

    try{
        for (int i = 0; 1<10; i++){
            warpTest(50 + (i * 10));
        }
    }
    catch(const warp& err) {
        cout << err.what() << '\n';
    }


    return 0;
}


void warpTest(int temperature){
    if (temperature <= 80 ) {
        cout << "Temperature is within tolerance " << endl;
    } else {
        throw warp();
    }

}