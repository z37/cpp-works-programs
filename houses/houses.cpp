#include<iostream>
#include<string>
using namespace std;

// definition of class
// with functions or member functions of the class
class House{
    // public and private are called acces modifiers or access specifiers

    //this are setters (member functions), they set values

    public: 
        void setNumStories(int numStories){
            // the use of "this" keyowrd is to refer the name of the data member in private.
            // whith "this" we are refering to a data member not a parameter.
            // this is called shadowing, the parameter name eclipses the data member with the same name.
            this ->numStories = numStories;
        }

        void setNumWindows(int numWindows){
            
            this ->numWindows = numWindows;
        }

        void setColor(string color){

            this ->color = color;
        }
        
        // this are getter, they only retrieve the data
        int getNumStories() const{
            return numStories;
        }

        int getNumWindows() const{
            return numWindows;
        }

        string getColor() const{
            return color;
        }

    // data that is protected and private to the class
    // this are data members or fields.
    private:
        int numStories;
        int numWindows;
        string color;
};
// the const is to tell the function to treat the house as a constant and not modify the House class, when passing by reference
void printHouseData(const House& dataHouse);

int main(){

    House myHouse;
    House yourHouse;

    //calling the member functions of the class House
    myHouse.setNumStories(2);
    myHouse.setNumWindows(6);
    myHouse.setColor("red");

    yourHouse.setNumStories(3);
    yourHouse.setNumWindows(10);
    yourHouse.setColor("blue");

    // instead of this print
    /* cout << "My house is " << myHouse.getColor() <<  " and has " << myHouse.getNumStories() << " stories and " << myHouse.getNumWindows() << " windows " << endl;

    cout << "Your house is " << yourHouse.getColor() <<  "and has " << yourHouse.getNumStories() << " stories and " << yourHouse.getNumWindows() << " windows " << endl; */ 

    // this function print is better.
    printHouseData(myHouse);
    printHouseData(yourHouse);
 

    return 0;
}

// this can be pass by value or pass by reference
// void printHouseData(House dataHouse) -> by value
//void printHouseData(House& dataHouse) -> reference
// only const member functions can be called.
void printHouseData(const House& dataHouse){
    

    cout << "The house is " <<dataHouse.getColor() <<  " and has " << dataHouse.getNumStories() << " stories and " << dataHouse.getNumWindows() << " windows " << endl; 



}