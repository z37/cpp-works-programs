#ifndef DICT_HPP
#define DICT_HPP

#include<string>
#include<map>
using namespace std;

class dict{
private:
    map<string, string> dictionary;
public:
    void addDefinition(string word, string definition);
    string getDefinition(string word) const;
    void printAll() const;

};

void dict::addDefinition(string word, string definition){

    auto it = dictionary.find(word);

    if (it != dictionary.end()){
        // already in the dictonary
        it->second = definition;
    } else {
        // new word
        dictionary[word] = definition;
    }
    

}

string dict::getDefinition(string word) const{
    auto it = dictionary.find(word);

    string result = "";

    if (it != dictionary.end()){
        result = it->second;
    } else {
        result = "NOT FOUND!";
    }
    return result;

}

void dict::printAll() const{

    for(auto pair : dictionary){
        cout << pair.first << ":\t" << pair.second << endl;
    }
    cout << endl;
}

#endif