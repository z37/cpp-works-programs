#include<iostream>
#include<memory>
#include"car.hpp"
using namespace std;

int main(){

    
    
    // using auto
    auto myCar = make_unique<car>("red", 4);
    
    
    cout << myCar->getColor() << endl;
    cout << myCar->getNumDoors() << endl;

    return
     0;
}
