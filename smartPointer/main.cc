#include<iostream>
#include<memory>
using namespace std;

int main(){

    const int ARRAY_SIZE = 5;

    // this is in c++ 11 standar
    // unique_ptr<double> myDblPtr(new double);

    // for the c++ 14 standar it´s used like this,
    unique_ptr<double> myDblPtr = make_unique<double>();
    
    // using auto
    auto myArray = make_unique<int[]>(ARRAY_SIZE);
    

    *myDblPtr = 4.21;

    cout << "pointer value : " << *myDblPtr << endl;

    unique_ptr<double> otherPtr = move(myDblPtr);

    cout << "other pointer value : " << *otherPtr << endl;

    // filling the array
    for (int i = 0; i < ARRAY_SIZE; i++){
        myArray[i] = i * 2;
    }
    
    // printing array
    for (int i = 0; i < ARRAY_SIZE; i++){
        cout << myArray[i] << endl;
    }


    return 0;
}
