#ifndef CAR_HPP
#define CAR_HPP

#include<string>
using namespace std;

class car{
private:
    string color;
    int numDoors;
public:
    car(string color, int numDoors);
    string getColor();
    int getNumDoors();
    
};

car::car(string color, int numDoors){
    this-> color    = color;
    this-> numDoors = numDoors;
}

string car::getColor(){
    return color;
}

int car::getNumDoors(){
    return numDoors;
}

#endif

