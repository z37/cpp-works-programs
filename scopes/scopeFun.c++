#include<iostream>
using namespace std;

void modifyGlobal();

int counter = 0;


void Fun1(int param1);
double myGolbalDbl = 3.1213;

int main(){

  /*   int localNumMain = 20;

    cout<<"The local to main variable is "<< localNumMain<<endl;
    cout<< "global double in main is "<<myGolbalDbl<<endl;

    Fun1(24); */

    cout<< "global counter in main before modifyGlobal call in for loop, is: "<<counter<<endl;      

    for (int i = 0; i < 100; i++)
    {
        modifyGlobal();
    }
    
     cout<< "global counter in main after modifyGlobal call in for loop, is: "<<counter<<endl;  




    return 0;
}


// the local variable is created and destroyed each time the function is called
void Fun1(int param1){

    int myLocalNum = 100;

    cout<<"my local number is "<<myLocalNum<<endl;
    cout<<"my parameter number is "<<param1<<endl;
    cout<<"my global double (in Fun1) is "<<myGolbalDbl<<endl;


}

void modifyGlobal(){
    counter++;
}