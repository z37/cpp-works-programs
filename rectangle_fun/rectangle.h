#ifndef RECTANGLE_h
#define RECTANGLE_h

/* looks very close to a UML class diagram
,------------------------------------------.
|Rectangle                                 |
|------------------------------------------|
|-length: double                           |
|-width : double                           |
|+Rectangle()                              |
|+Rectangle(length: double, width : double)|
|+getLength()                              |
|+getWidth()                               |
|+setLength(length: double) : void         |
|+setWidth(width: double) : void           |
|+area()                                   |
|+preimeter()                              |
`------------------------------------------'
*/
class rectangle{
    public:
    
        // const in some functions, so that they do not modify internal data(private data member; length and width)

        rectangle();
        rectangle(double length, double width);
        double getLength() const; // const for no modification of the internal data
        double getWidth() const;
        void setLength(double length);
        void setWidth(double width);
        double area() const;
        double perimeter() const;


    private:
        double length;
        double width;

};


#endif