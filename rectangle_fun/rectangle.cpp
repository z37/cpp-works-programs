#include<iostream>
#include"rectangle.h"


// data members
rectangle::rectangle(){

    this-> length  = 1.0;
    this-> width   = 1.0;

}


// member functions

rectangle::rectangle(double length, double width){

    this-> length = length;
    this-> width  = width;
}

double rectangle::getLength() const{
    return length;
}

double rectangle::getWidth() const{
    return width;
}

void rectangle::setLength(double length) {
    this-> length = length;
}

void rectangle::setWidth(double width) {
    this-> width = width;
}

double rectangle::area() const{

    return length * width;
}

double rectangle::perimeter() const{
    return 2 * (length + width);
}

