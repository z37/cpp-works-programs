#include <iostream>
#include "rectangle.h"
using namespace std;



int main(){

    // this creates instance of the class
    // which is rectangle

    rectangle r1; // unit rectangle
    // rectangle of {length,width = 5,2}
    rectangle r2(5, 2);

    cout << "r1 area is " << r1.area() << endl; 
    cout << "r1 perimeter is " << r1.perimeter() << endl; 
    cout << "r2 area is " << r2.area() << endl; 
    cout << "r2 perimeter is " << r2.perimeter() << endl;    

    r1.setLength(22); // changing length = 22
    r1.setWidth(12);  // changin wodth = 12
    cout << "r1 after changin length and width" << endl;
    cout << "r1 length is " << r1.getLength() << endl; 
    cout << "r1 width is " << r2.getWidth() << endl; 

    cout << "r1 area is NOW " << r1.area() << endl;     
    cout << "r1 preimeter is NOW " << r1.perimeter() << endl; 



    return 0;
}