#ifndef node_hpp
#define node_hpp

class node{
private:
    int data;
    node* next;
public:
    node(int data, node* next);
    void setData(int data);
    void setNext(node* next);
    int getData() const;
    node* getNext() const;
    
};

node::node(int data, node* next){
    this-> data = data;
    this-> next = next;
}

void node::setData(int data){
    this-> data = data;

}

void node::setNext(node* next){
    this-> next = next;
}

int node::getData() const{
    return data;
}

node* node::getNext() const{
    return next;
}


#endif