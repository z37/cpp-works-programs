#include<iostream>
#include"node.hpp"
using namespace std;

node* createChain();
void deleteChain(node*& head);
void printChain(node*& head);

int main(){

    node* theHead;

    theHead = createChain();

    printChain(theHead);
    deleteChain(theHead);

    return 0;
}

node* createChain(){
    node* head = nullptr;

    // in here we are adding at the front of the list the
    // and it is growing from that frint all the way down the linked chain
    for (int i = 0; i < 25; i++){
        head = new node(i, head);
    }
    return head;

}

void deleteChain(node*& head){
    node* nodeToDelete;

    while (head != nullptr){
        nodeToDelete = head;
        head         = head->getNext();
        delete nodeToDelete;
    }
    
}

void printChain(node*& head){
    node* walker = head;
    int counter = 0;
    while (walker != nullptr){
        cout << walker->getData() << endl;
        walker = walker->getNext();
        counter++;
    }
    cout << "Number of elements in linked chain: " << counter << endl;
    
}