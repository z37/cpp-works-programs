#include<iostream>
#include<fstream>
#include<vector>
using namespace std;

int main(){

    // ifstream for input file
    ifstream infile;
    infile.open("input.txt");
    vector<int> nums;


    int inputNum;
    int sum = 0;

    // keep the while until the end of file is reached.
    while (!infile.eof()){
        infile >> inputNum;
        nums.push_back(inputNum);
        sum += inputNum;
    }

    cout << "Sum of the number is :" << sum << endl;

    for (auto i : nums){
        cout << i << endl;
    }
    
    
    infile.close();

    return 0;
}