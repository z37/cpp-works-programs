#ifndef linkedlist_hpp
#define linkedlist_hpp

#include<iostream>
#include"list.hpp"
using namespace std;


//------- Node class --------------//
class node
{
private:
    int data;
    node* next;
public:
    node(int data, node* next);
    int getData() const;
    void setData(int data);
    node* getNext() const;
    void setNext(node* next);

};

node::node(int data, node* next){
    this-> data = data;
    this-> next = next;
}


int node::getData() const {
    return data;
}

void node::setData(int data){
    this-> data = data;
}

node* node::getNext() const{
    return next;

}

void node::setNext(node* next){
    this->next = next;
}


//------- Linked List class --------------//

class linkedList{ // remember that since, we have pure virtual methods we cannot instantiate the class List directly in main
private:
    node* mHead;
    int mNumElements;

public:
    linkedList(){
        mNumElements = 0;
        mHead        = nullptr;
    }

    virtual ~linkedList(){
        makeEmpty();
    }

    void add(int newEntry){
        node* newNode = new node(newEntry, mHead); // next = mHead;
        mHead = newNode;

        mNumElements++;
    }

    void add(int newEntry, int position){
        node* newNode = new node(newEntry, mHead);

        if (position >= mNumElements + 1 || position < 0){
            cout << "Error: cannot add at speficied position" << endl;
            return; // get out of here
        }
        
        if (position == 0){
            newNode->setNext(mHead);
            mHead = newNode;
        } else {
            // not beginnig
            node* nodeBefore = mHead;
            node* nodeAfter;
            
            for (int i = 0; i < position - 1; i++){
                nodeBefore = nodeBefore->getNext();
            }// end for

            nodeAfter = nodeBefore->getNext();
            newNode->setNext(nodeAfter);
            nodeBefore->setNext(newNode);
        }

        mNumElements++;
    }

    void set(int newEntry, int position){
        node* walker = mHead;
        int index    = 0;

        if (position >= mNumElements || position < 0){
            cout << "Error: cannot set at speficied position" << endl;
            return; // get out of here
        }

        for (int i = 0; i < position; i++){
            walker = walker->getNext();
        }

        walker->setData(newEntry);
        
    }


    bool contains(int entry) const{
        bool result = false;

        result = find(entry) != -1; // true if it is found

        return result;
    }

    int find(int entry) const{
        int foundIndex = -1; // assume it is not found

        int indexCounter = 0;
        node* walker = mHead;

        while (walker != nullptr){
            if (walker->getData() == entry){
                foundIndex = indexCounter;
                break;
            }
            indexCounter++;
        }
        return foundIndex;
    }

    int remove(int position){
        if (position >= mNumElements || position < 0){
            cout << "Error: cannot remove at speficied position" << endl;
            return 0; 
        }

        int dataToReturn = 0;

        if (position == 0){
            node* temp   = mHead;
            dataToReturn = temp->getData();
            mHead        = mHead->getNext();
            delete temp; 
        } else {
            node* nodeBefore = mHead;
            node* nodeToRemove;
            node* nodeAfter;

            for (int i = 0; i < position - 1; i++){
                nodeBefore = nodeBefore->getNext();
            }
            nodeToRemove = nodeBefore->getNext();
            dataToReturn = nodeToRemove->getData();

            nodeAfter = nodeToRemove->getNext();

            nodeBefore->setNext(nodeAfter);

            delete nodeToRemove;
            
        }
        
        mNumElements--;
        
        return dataToReturn;
    }

    void makeEmpty() {
        node* temp;
        while (mHead != nullptr){
            temp = mHead;
            mHead = mHead->getNext();
            delete temp;
        }
       mNumElements = 0; 
    }

    int size() const {
        return mNumElements;
    }

    bool isEmpty() const {
        return mNumElements == 0;
    }

    void printList() const {
        node* walker = mHead;
        
        while (walker != nullptr){
            cout << walker->getData() << endl;
            walker = walker->getNext();
        }
    }

    
};


#endif 