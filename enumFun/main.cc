#include<iostream>
using namespace std;

int main(){
        // values   0   1       2   3       4
    enum Direction{up, down, left, right, standing};

    Direction myDirection = standing;

    cout << myDirection << endl;

    if (myDirection == up){
        cout << "up " << endl;
    } else if (myDirection == down){
       cout << "down " << endl;
    } else if (myDirection == left){
        cout << "left " << endl;
    } else if (myDirection == right){
       cout << "right " << endl;
    } else if (myDirection == standing){
        cout << "standing still! " << endl;
    }
    
    

    return 0;
}