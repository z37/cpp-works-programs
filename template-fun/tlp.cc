#include<iostream>
#include<string>
#include"swapper.hpp"
using namespace std;

// templates are 2
// function and class

// template function
// template class T, is just a conventions
template <class T>
T getBigger(T a, T b);

template<class T>
T getSmaller(T a, T b);



/* also get rid of function prototypes
double getBigger(double a, double b);
int getBigger(int a, int b);
string getBigger(string a, string b);
*/

int main(){


    // using swapper template
    swappper<int> intSwapper(5, 10);
    swappper<string> stringSwapper("Jhon", "Bob");

    string name1 = "Jhon";
    string name2 = "Alice";

    double  biggerDub = getBigger(3.14, 5.55);
    int biggerInt     = getBigger(11, 9);
    string biggerStr  = getBigger(name1, name2);

    cout << "Bigger items: "<< endl;
    cout << "\t "<<biggerDub <<endl;
    cout << "\t "<<biggerInt <<endl;
    cout << "\t "<<biggerStr <<endl;
    cout << endl << endl;

    cout << "Testing the swapper! "<< endl;
    cout << intSwapper.getFirst() << " " << intSwapper.getSecond() << endl;
    cout << stringSwapper.getFirst() << " " << stringSwapper.getSecond() << endl;
    cout << endl << endl;

    intSwapper.swap();
    stringSwapper.swap();

    cout << "After the swapper! "<< endl;
    cout << intSwapper.getFirst() << " " << intSwapper.getSecond() << endl;
    cout << stringSwapper.getFirst() << " " << stringSwapper.getSecond() << endl;
    cout<< endl<< endl;

    double  smallerDouble = getSmaller(3.14, 5.55);
    int smallerInt  = getSmaller(11, 9);
    string smallerStr  = getSmaller(name1, name2);

    cout << "Smaller items: "<< endl;
    cout << "\t "<<smallerDouble <<endl;
    cout << "\t "<<smallerInt <<endl;
    cout << "\t "<<smallerStr <<endl;
    cout << endl << endl;


    return 0;
}

// template
template <class T>
T getBigger(T a, T b){
    if (a > b) {
        return a;
    } 
        return b;

}

template <class T>
T getSmaller(T a, T b){
    if (a < b) {
        return a;
    } 
        return b;

}




/* Instead of using the function overloading for the same function, we can use template function.
double getBigger(double a, double b){
    if (a > b) {
        return a;
    } 
        return b;
    
}
int getBigger(int a, int b){
    if (a > b) {
        return a;
    }
        return b;
}
string getBigger(string a, string b){
    if (a > b) {
        return a;
    }
        return b;
}
*/