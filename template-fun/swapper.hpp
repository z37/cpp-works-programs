#ifndef SWAP_HPP
#define SWAP_HPP

template <class T>
class swappper{
private:
    T first;
    T second;

public:
    swappper(T first, T second);
    void swap();
    T getFirst() const;
    T getSecond() const;
};


// this is the syntax, telling that this is swapper class T
template <class T>
swappper<T>::swappper(T first, T second){
    this-> first = first;
    this-> second = second;

}

template <class T>
void swappper<T>::swap(){
    T temp /*this is a flag*/ = first;
    first = second;
    second = temp;

}

template <class T>
T swappper<T>::getFirst() const{
    return first;
}

template <class T>
T swappper<T>::getSecond() const{
    return second;

}



#endif