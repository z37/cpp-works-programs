
/*this are include guards.
 use preprocessors before the compilation
 they are added at the top of .h file 
 #ifndef (if not defined) followed by a constant name (house_h)
 #define house_h


*/
#ifndef house_h //like an if statement to check if house_h exists so it is not define

#define house_h // then it defines it here


#include<iostream>
#include<string>
using namespace std;

class House{
    public: 
        void setNumStories(int numStories);
        
        void setNumWindows(int numWindows);

        void setColor(string color);

        void print() const;
    
        int getNumStories() const;

        int getNumWindows() const;

        string getColor() const;
        
    private:
        int numStories;
        int numWindows;
        string color;
};

#endif