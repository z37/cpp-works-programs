#include"house.hpp"
#include<iostream>
using namespace std;


// not a global function, it belogns to the class specified 
// in house.hpp, with house::name_function ... 
void House::setNumStories(int numStories){
    
    this ->numStories = numStories;
    }

void House::setNumWindows(int numWindows){
    
    this ->numWindows = numWindows;
    }

void House::setColor(string color){
    
    this ->color = color;
    }
        
int House::getNumStories() const{
    return numStories;
    }

int House::getNumWindows() const{
    return numWindows;
    }

string House::getColor() const{
    return color;
    }

void House::print() const{
    
    cout << "The house is " << color <<  " and has " << numStories << " stories and " << numWindows << " windows " << endl; 


}