#include<iostream>
#include<string>
#include"house.hpp"
using namespace std;


int main(){

    House myHouse;
    House yourHouse;

    //calling the member functions of the class House
    myHouse.setNumStories(123);
    myHouse.setNumWindows(6);
    myHouse.setColor("red");

    yourHouse.setNumStories(3);
    yourHouse.setNumWindows(10);
    yourHouse.setColor("blue");


    // this function print is better.
    myHouse.print();
    yourHouse.print();
 

    return 0;
}
