#include<iostream>
#include<array>
using namespace std;

int sumArrayobjct(array<int, 10> arrayObj);
void sumArrayobjct(array<int, 10> arrayObj,int& totalSum);

int main(){
    
    array<int, 10> myArrayObj{2,4,6,8,10,12,14,16,18,20};

    cout << "the sum of the elemnts in the array is: "<< sumArrayobjct(myArrayObj) << endl;

    int sumResult;

    sumArrayobjct(myArrayObj, sumResult);

    cout << "the sum of the elemnts in the array object by reference os: " << sumResult
    << endl;


    return 0;
}


int sumArrayobjct(array<int, 10> arrayObj)
{
    int sum = 0;
    for (auto array : arrayObj)
    {
        sum += array;
    }
    return sum;
    
}
void sumArrayobjct(array<int, 10> arrayObj,int& totalsum)
{
    for (auto array : arrayObj)
    {
        totalsum += array;
    }
}