#include<iostream>
#include<array>
using namespace std;

const int Array_size = 5;
array<int, Array_size> myArray = {1,2,3,4,5};
int sumArray(int size_array);
int otroSumArray(int myArray[],int arraySize);

int main(){
    
   cout << "the sum of the elemnts in the array is: "<< sumArray(Array_size)<< endl;

   cout<<endl<<endl;

   int myArray2[5]{1,2,3,4,5};
   int sum = otroSumArray(myArray2, 5);
   
   cout << "the sum of the elemnts in the array is: "<< sum << endl;

    return 0;
}

int sumArray(int size_array){
    int sumArray = 0;
    for (int i = 0; i < Array_size; i++)
    { 
        sumArray += myArray[i];
    }

    return sumArray;
    
}

int otroSumArray(int myArray[],int arraySize)
{
    int sum = 0;
    for (int i = 0; i < arraySize; i++)
    {
        sum += myArray[i];
    }
    return sum;
}