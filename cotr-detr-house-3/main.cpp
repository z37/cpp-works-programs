#include<iostream>
#include<string>
#include"house.hpp"
using namespace std;


int main(){


    // when the object is created the constructor is called automatically
    // on the contrary the destructor is called automatically when the object is detroyed 
    House myHouse;
    House yourHouse;

    // called of the constructor 2, looks like a function declaration of type of the class House
    House thierhouse(1, 3, "green");

    // the constructor is called automatic when the object is created

    // when they go out of the scope the destrcutors are called automatically


    myHouse.print();


    //print of the 2nd constructor
    thierhouse.print();


    //calling the member functions of the class House
    myHouse.setNumStories(123);
    myHouse.setNumWindows(6);
    myHouse.setColor("red");

    yourHouse.setNumStories(3);
    yourHouse.setNumWindows(10);
    yourHouse.setColor("blue");


    // this function print is better.
    myHouse.print();
    yourHouse.print();
 

    return 0;
}
