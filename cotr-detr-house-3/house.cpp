#include"house.hpp"
#include<iostream>
using namespace std;

// no argument constructor 
// other name .- no ARG constructor
House::House(){
    this-> numStories = 1;
    this->numWindows  = 4;
    this->color       ="white";

}

// destructor
House::~House(){

    cout << "The" << color << " house with " << numStories << " stories" << " and " << numWindows << " windows is being destroyed" << endl;
}

// constructor 2
House::House(int numStories, int numWindows, string color){
    this-> numStories = numStories;
    this->numWindows  = numWindows;
    this->color       = color;

}


// not a global function, it belogns to the class specified 
// in house.hpp, with house::name_function ... 
void House::setNumStories(int numStories){
    
    this ->numStories = numStories;
    }

void House::setNumWindows(int numWindows){
    
    this ->numWindows = numWindows;
    }

void House::setColor(string color){
    
    this ->color = color;
    }
        
int House::getNumStories() const{
    return numStories;
    }

int House::getNumWindows() const{
    return numWindows;
    }

string House::getColor() const{
    return color;
    }

void House::print() const{
    
    cout << "The house is " << color <<  " and has " << numStories << " stories and " << numWindows << " windows " << endl; 


}