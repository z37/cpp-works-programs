#include<iostream>
#include<fstream>
#include<vector>
using namespace std;

int main(){

    ifstream ageFile;
    ofstream outFile;
    int inputNum;

    vector<int> doublNum;

    cout << "opening files ..." << endl;

    ageFile.open("ages.txt");
    outFile.open("output.txt");

    if (!ageFile){
        cout << "problem opening the file ... aborted" <<  endl;
        return 1;
    }

    while (!ageFile.eof()){
        ageFile >> inputNum;
        outFile << inputNum << " " << inputNum * inputNum << endl;
    }

    ageFile.close();
    outFile.close();
    

    return 0;
}
