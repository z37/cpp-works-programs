#include<iostream>
#include"alien.hpp"

using namespace std;

int main(){


    alien alien1(120, 50, 'M');
    alien alien2(200, 55, 'M');
    alien alien3(150, 60, 'M');

    alien alien4 = alien1 + alien2;
    alien alien5 = alien2 + alien3;

    cout << "alien2 == alien2? " << boolalpha << (alien1 == alien2) << endl;
    cout << "alien1 != alien3? " << boolalpha << (alien1 != alien3) << endl;
    cout << "alien4 > alien5? " << boolalpha << (alien4 > alien5) << endl;
    cout << "alien4 >= alien5? " << boolalpha << (alien4 > alien5) << endl;
    cout << "alien4 < alien5? " << boolalpha << (alien4 < alien5) << endl;
     cout << "alien4 <= alien5? " << boolalpha << (alien4 <= alien5) << endl;
    


    return 0;
}