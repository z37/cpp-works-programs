#ifndef alien_hpp
#define alien_hpp

#include<string>
#include<cstdlib>
#include<ctime>
using namespace std;

class alien {
private:
    int weight;
    int height;
    char gender;
public:
    alien(int weight, int height, char gender);
    int getWeight() const;
    int getHeight() const;
    char getGender() const;
    int getPrestige() const;

    bool operator==(alien& other) const; 
    bool operator!=(alien& other) const; 
    bool operator>(alien& other) const; 
    bool operator>=(alien& other) const; 
    bool operator<(alien& other) const; 
    bool operator<=(alien& other) const; 
    alien operator+(alien& other) const; 
    void operator=(alien& other); 

};

alien::alien(int weight, int height, char gender){
    this->weight = weight;
    this->height = height;
    this->gender = gender;
}

int alien::getWeight() const{
    return weight;
}

int alien::getHeight() const{
    return height;
}

char alien::getGender() const{
    return gender;
}

int alien::getPrestige() const{
    int genderValue;

    if (gender == 'M'){
        genderValue = 2;
    } else {
        genderValue = 3;
    }
    return weight * height * genderValue;
}

bool alien::operator==(alien& other) const{

    return getPrestige() == other.getPrestige();
}

bool alien::operator!=(alien& other) const{
    return getPrestige() != other.getPrestige();
}

bool alien::operator>(alien& other) const{
    return getPrestige() > other.getPrestige();
}

bool alien::operator>=(alien& other) const{
    return getPrestige() >= other.getPrestige();
} 

bool alien::operator<(alien& other) const{
    return getPrestige() < other.getPrestige();
}

bool alien::operator<=(alien& other) const{
    return getPrestige() <= other.getPrestige();
}

alien alien::operator+(alien& other) const{
    srand(time(nullptr));

    int randNum = rand() % 10; // 1 through 10 inclusive

    char newGender;

    int newWeigth = (weight + other.weight) / 2;
    int newHeight = (height + other.height) / 2;

    if (randNum < 4){
        newGender = 'F';
    } else {
        newGender = 'M';
    }
    return alien(newHeight, newWeigth, newGender);
    
}

void alien::operator=(alien& other){
    weight = other.weight;
    height = other.height;
    gender = other.gender;
}



#endif