#include<iostream>
#include"rectangle.hpp"
#include"rectangle_helper.hpp"
using namespace std;

int main(){

    rectangle_helper helper;

    rectangle r1(10, 25);
    cout << "r1 area: " << r1.area() << endl;

    changeData(r1);

    cout << "now, r1 area: " << r1.area() << endl;


    helper.modifyRectangle(r1);

    cout << "After class friend rectangle_helper, r1 area: " << r1.area() << endl;

    cout << "Now r1 values are; width and length"<< endl;
    printData(r1);

    return 0;

}

void changeData(rectangle& rect){
        rect.width = 100;
        rect.length = 100;

}

void printData(rectangle& rect){

    cout <<"width:  " << rect.width << endl;
    cout << "lenth: " << rect.length << endl;
}