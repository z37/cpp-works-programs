#include<iostream>
#include<string>
#include<map>
using namespace std;

int main(){

    map<string, string> contacts;

    contacts["Jhon Baugh"] = "333-333-3333";
    contacts["Bob Ross"]   = "434-343-2323";
    contacts["Ross"]       = "444-143-1123";


    // instead  of pair<string, string> we can use auto 
    for (auto element : contacts){
        cout << element.first << " = " << element.second<< endl;
    }


    return 0;
}