/*
    arithmetic operatos
    + -> addition operator
    - -> substraction operator
    * -> multiplication ''
    / -> division ''
    % -> modulus ''
*/

#include<iostream>
using namespace std;


int main(){

    int a = 10;
    int b = 3;
    int sum = a + b;
    int diff = a - b;
    int product = a * b;
    int quotient = a / b;
    int remainder = a % b;

    int result = 10;
    result += 10; // result = result + 10;

    int myInt = 5;
    
    cout << "Sum = " << sum << endl;
    cout << "Difference =" << diff << endl;
    cout << "Product =" << product << endl;
    cout << "Quotient =" << quotient << endl;
    cout << "Remainder =" << remainder << endl;
    cout << "Result =" << result << endl;

    cout << myInt << endl;
    myInt++;  // similar to ++myInt; and myInt+=1;
    cout << myInt << endl;

    myInt--; // similar to --myInt; and myInt-=1;
    cout << myInt << endl;

    return 0;   
}