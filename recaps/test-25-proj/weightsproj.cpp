#include <iostream>
#include <vector>
#include<string>
using namespace std;

int main(){

    const int nums_People = 5;
    vector<string> vecNames;
    vector<int> vecWeights;
    string tempName;
    int tempWeight;

    
    for(int i=0;i<nums_People;i++){

        cout<<"Enter the name "<<endl;
        getline(cin, tempName);

        cout<<"Enter the weight of  " << tempName << endl;
        cin >> tempWeight;
        cin.get(); // consume newline char

        // append to vector names and weights

        vecNames.push_back(tempName);
        vecWeights.push_back(tempWeight);


    }
    
    cout<<endl <<endl;

    for(int i=0;i<nums_People;i++){

        cout<< vecNames[i] << " weights " << vecWeights[i] << " lbs" <<endl;

    }

    return 0;


}