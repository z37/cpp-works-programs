#include<iostream>
using namespace std;

// functions prototypes
void printHello();
void printNumber(int a);
int giveMe10();
int addThese(int num1,int num2);

int square(int number);


int main(){

    int num1 = 120;
    int num2 = 500;

    printHello();
    printNumber(100);
    
    int someData = giveMe10();
    cout<< someData << endl;

    int totalValue = addThese(num1,num2);
    cout<< totalValue <<endl;

    //functions can be mixed up

    printNumber(addThese(num1,50));

    //square funcition
    cout<< "7^2 is " << square(7) <<endl;


    return 0;

}

void printHello(){
    cout<< "Hello World"<< endl;
}

void printNumber(int a){
    cout<< "the number a is" << a <<endl;
}

int giveMe10(){
    return 10;
}

int addThese(int num1,int num2){
    return num1 + num2;
}

// project function square
int square(int number){
    return number*number;
}


