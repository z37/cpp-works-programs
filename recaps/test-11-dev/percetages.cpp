#include<iostream>
#include<string>

using namespace std;

int main(){

    string fullName;
    string location;
    int initialScore;

    cout << "Please enter the user's full name :" << endl;
    getline(cin,fullName);

    cout << "Please enter the user's city, state/province, and country :" << endl;
    getline(cin,location);

    cout << "Please enter your project score :" << endl;
    cin >> initialScore;

    cout << "Hello, " << fullName << endl;
    cout << "We heard you are from, " << location << endl;
    cout << "Your initial score is, " << initialScore << endl;
    cout << "But with 5 points your score is now, " << (initialScore + 5)<< endl;



    return 0;
}