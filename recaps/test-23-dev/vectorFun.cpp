#include <iostream>
#include <vector>
#include<string>
using namespace std;

int main(){

    vector<int> vec1;
    vector<string> vec2(3);

    vec1.push_back(1);
    vec1.push_back(2);
    vec1.push_back(3);

    cout << "vec1 size " <<vec2.size() <<endl;

    vec2[0] = "Jhon";
    vec2[1] = "Bob";
    vec2[2] = "Sally";

    vec2.push_back("Shannon");

    for(int val : vec1){

        cout<< val << endl;
    }
    cout<<endl;

    for(auto name : vec2){

        cout<< name << endl;
    }

    cout<< "Front and back" << endl;
    cout<< "front: " << vec2.front() << endl;

    vec2.pop_back(); //removes elemnt on the back
    vec2.insert(vec2.begin(), "Don"); // inserts Don at the start of the vector

    cout<< "now, front is: " << vec2.front() << endl;
    cout<< "now, back is: " << vec2.back() << endl;



    return 0;


}