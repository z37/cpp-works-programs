#include <iostream>
#include <vector>
#include<string>
using namespace std;

int main(){

    vector<string> vecNames;

    vecNames.push_back("Jhon");
    vecNames.push_back("popo");
    vecNames.push_back("lalas");
    vecNames.push_back("des");
    vecNames.push_back("lop");

    vecNames.insert(vecNames.begin()+2, "Jhon Baugh"); 
    vecNames.pop_back(); 

    for(auto name : vecNames)
    {
        cout << name << endl;
    }


    return 0;


}