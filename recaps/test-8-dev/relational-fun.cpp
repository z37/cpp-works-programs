#include <iostream>
using namespace std;

/*
    >   greater than
    >=  greater than or equal to
    <   less than
    <=  less than or equal to
    ==  equal to (equality)
    !=  not equal to (different from)

*/

int main(){

    cout << boolalpha; //set 0 and 1 to false, true

    int a = 15;
    int b = 20;
    bool areEqual = a == b;
    int myAge = 20;


    cout << (a < b) << endl;

    cout << areEqual << endl;

    cout << "Age >= 21? " 
    << (myAge >= 21) << endl;

    return 0;
}