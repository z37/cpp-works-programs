#include<iostream>
using namespace std;

int main(){

    const int array_size = 10;
    int myArray[array_size];

    for(int i=0; i<array_size; i++){
        myArray[i] = i + 1;
    }

    for(int index : myArray){
        cout<< index << endl;
    }

    return 0;
}