#include<iostream>
#include<string>
using namespace std;

int main(){

    const int array_size = 5;

    int myArray[array_size];
    string nameArray[4]{"Bob","Sally","Jhon","Ed"};

    myArray[0] = 15;
    myArray[1] = 20;
    myArray[2] = 22;
    myArray[3] = 13;
    myArray[4] = 6;

    for(int i=0; i<array_size; i++){

        cout << myArray[i] <<endl;

    }

    /* for(int i=0; i<4; i++){

        cout << nameArray[i] <<endl;

    } */

    // auto -> tells c++ to infer the data type, like python
    for(auto name : nameArray){
        cout<< name <<endl;
    }


    return 0;
}