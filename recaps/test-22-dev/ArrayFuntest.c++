#include<iostream>
#include<array>
using namespace std;

int main(){

    array<int, 5> my_int_array{};

    my_int_array[0] = 1;
    my_int_array[1] = 2;
    my_int_array[2] = 3;
    my_int_array[3] = 4;
    my_int_array[4] = 5;

    for(int a : my_int_array){
        cout<< a << endl;
    }

    cout<< "The size of the array is "<< my_int_array.size() <<endl;

    return 0;
}