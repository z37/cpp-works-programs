#include <iostream>
using namespace std;

int main(){
    
    int count = 0;

    while (count <= 50){

        if (count % 2 == 0){
            cout<< count << " is even"<< endl;
        }
        else{
            cout<< count << " is odd"<< endl;
        }

        count++;

    }

    return 0;

}