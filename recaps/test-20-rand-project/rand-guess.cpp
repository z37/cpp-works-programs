#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(){

    srand(time(nullptr));

    bool guessNumber = false;
    int count = 1;
    int userGuess;
    int pcNumber = rand() % 100 + 1; //computer number guess


    while (!guessNumber){
        
        cout<< " Guess a number between 0 and 100"<< endl;
        cin>>userGuess;

        if (userGuess > 100 || userGuess < 0  ){
            cout<< " Error, please type a number between 0 and 100"<< endl;
            count++;
            continue;
        }
        else if (userGuess  == pcNumber){
            cout<< " Great! you guess the number in "<< count << " guesses!" <<endl;
            guessNumber = true;
        }
        else if (userGuess > pcNumber){
            cout<< " The number entered is too high!"<< endl;
        }
        else {
            cout<< " The number entered is too low!"<<endl;
        }
        count++;
    }

    return 0;

}