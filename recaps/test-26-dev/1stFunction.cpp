#include <iostream>
using namespace std;

// header prototype
void printSomething(); // declarations of the functions to tell main, with no body
void myName();


int main(){

    printSomething();// this is calling or invoking the function.

    cout<<endl;

    myName();

    cout<<endl;

    return 0;
}

//1st function 
// this is the function definition
// 1st  part is the function header is the return type eg. void -> no return. The identifier -> name of function. Parentheses are the parameters list or argument list

//2nd is the body, for what the function does. 
void printSomething(){

    cout<<"hey, look my first function"<<endl;
}

// function print my name
void myName(){

    cout<<"My names jara jara"<<endl;
}