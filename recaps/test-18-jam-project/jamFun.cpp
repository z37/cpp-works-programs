#include<iostream>
using namespace std;

int main(){
    
    char package; //package selected
    int totalJams; // total of jams, depending each case
    int numJams; // number oj jams purchased
    int jamsIncluded; // jams included in each package
    int monthFee; // fee of the included jams


    cout<<"Please enter the letter of the package you have (A,B or C) "<<endl;
    cin>>package;

    cout<<"Please enter the total number of jams purchased this month"<<endl;
    cin>>numJams;

    switch (package)
    {
    case 'A':
    case 'a':
        cout<<"Package A selected"<<endl;
        if (numJams > 2 ){
            jamsIncluded = 2;
            monthFee = 8;
            totalJams = numJams-jamsIncluded;
            totalJams *= 5;
            cout<< "The total is $" << (totalJams+monthFee)<< " usd" <<endl;
        }
        else {
            cout << "The total is $8 usd."<< endl;
        }
        break;
    case 'B':
    case 'b':
         cout<<"Package B selected"<<endl;
        if (numJams > 4 ){
            jamsIncluded = 4;
            monthFee = 12;
            totalJams = numJams-4;
            totalJams *= 4;
            cout<< "The total is $" << (totalJams+monthFee)<< " usd" <<endl;
        }
        else {
            cout << "The total is $12 usd."<< endl;
        }
        break;
    case 'C':
    case 'c':
         cout<<"Package C selected"<<endl;
        if (numJams > 6 ){
            jamsIncluded = 6;
            monthFee = 15;
            totalJams = numJams-6;
            totalJams *= 3;
            cout<< "The total is $" << (totalJams+monthFee)<< " usd" <<endl;
        }
        else {
            cout << "The total is $15 usd."<< endl;
        }
        break;
    default:
        cout<<"you have entered an invalid grade. Try again"<<endl;
    }

    return 0;

}