#include<iostream>
using namespace std;

int main(){

    int myArrays[2][3]{
        {1,2,3},
        {4,5,6}
    }; // 2D - array 2 x 3

    cout<< myArrays[0][2]<<endl;

    myArrays[1][0] = 14;

    cout<<myArrays[1][0]<<endl;

// nested for loop to print all values of matrix

    for(int row=0;row<2;row++){
            for(int col=0;col<3;col++){
                
                cout<< myArrays[row][col]<< " ";
            }
            cout<<endl;
    }

// reversed loop
    for(int row=1;row>=0;row--){
            for(int col=2;col>=0;col--){
                
                cout<< myArrays[row][col]<< " ";
            }
            cout<<endl;
    }

    return 0;
}