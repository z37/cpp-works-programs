#include<iostream>
using namespace std;

int main(){
    
    char grade;

    cout<<"Please enter a letter grade "<<endl;
    cin>>grade;

    switch (grade)
    {
    case 'A':
    case 'a':
        cout<<"Great job!"<<endl;
        break;
    case 'B':
    case 'b':
        cout<<"Great job!"<<endl;
    case 'C':
    case 'c':
        cout<<"You can do better!"<<endl;
    case 'D':
    case 'd':
        cout<<"Getting close to failing!"<<endl;
    case 'F':
    case 'f':
        cout<<"Failure!"<<endl;

        break;
    default:
        cout<<"you have entered an invalid grade. Try again"<<endl;
    }

    return 0;

}