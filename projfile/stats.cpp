#include<iostream>
#include<fstream>
using namespace std;


int main (){

    ifstream infile;

    infile.open("scores.txt");
    
    int inputNum;

    double sum = 0.0; 
    int count = 0;
    int  max = 0;
    int min = 0;
    double avg = 0;

    if (!infile){
        cout << "Error opening the file. " << endl;
        return 1;
    }

    while (!infile.eof()){
        infile >> inputNum;

        if (count == 0){
            min = inputNum;
            max = inputNum;
        }

        if (inputNum < min){
            min = inputNum; 
        }

        if (inputNum > max){
            max = inputNum;
        }
        sum += inputNum;
        count++;
    }


    if (count != 0)
    {
        avg = sum / count;
    }

    cout << "Stats: \n";
    cout << "max: " << max << endl;
    cout << "min: " << min << endl;
    cout << "avg: " << avg << endl;

    infile.close();
    



    return 0;
 }